import argparse

parser = argparse.ArgumentParser(add_help=False)

parser.add_argument('-o', '--operation',
                    help="The operation to perform on numbers.",
                    choices=['add', 'sub', 'mul', 'div'], default='add')
parser.add_argument("-v", "--verbose", action="store_true",
                    help="increase output verbosity")

# parser.add_argument('--operation', choices=['sftp', 'ftp'], default='ftp')

results = parser.parse_args()

print('operation     =', results.operation)
print('verbose   =', results.verbose)
# print('operation   =', results.operation)
