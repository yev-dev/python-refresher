
import itertools as it

first_names = ['Steve', 'Jane', 'Sara', 'Mary','Jack','Bob', 'Bily', 'Boni', 'Chris','Sori', 'Will', 'Won','Li']


def chunks(generator, step):
    """Yield successive chunks from a generator"""
    chunk = []

    for item in generator:
        if len(chunk) >= step:
            yield chunk
            chunk = [item]
        else:
            chunk.append(item)

    if chunk:
        yield chunk


# print(list(chunk(first_names, 2)))

n = 2

# output_list = [print(first_names[i:i+n]) for i in range(0, len(first_names), n) ]

# print(output_list)

chunk_size = 2

for chunk in  chunks(first_names, chunk_size):
    print(chunk)
