
import pprint as pprint


class Vehicle(object):

    def __init__(self, message, var):
        self._var = var
        self._name = message

    @property
    def message(self):
        return self._name

    @message.setter
    def set_message(self, message):
        self._name = message

    @property
    def var(self):
        print(self._var)
        return self._var

    @var.setter
    def var(self, var):
        self._var = var

    #
    # @var.deleter
    # def var(self):
    #     print(self._var)
    #     self._var = None

    def __repr__(self):
        return "{} {}".format(self._name, self._var)


name = 'Dina'
v = Vehicle(name, 'var1')


class Car(Vehicle):
    pass


c = Car(name, 'var2')

print(type(v))
print(type(c))

print(v.__class__)


