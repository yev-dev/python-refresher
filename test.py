
def gen():
    for i in range(0,20):
        yield i

def chunks(generator, step):
    """Yield successive chunks from a generator"""
    chunk = []

    for item in generator:
        if len(chunk) >= step:
            yield chunk
            chunk = [item]
        else:
            chunk.append(item)

    if chunk:
        yield chunk


chunk_size = 2

for chunk in  chunks(gen(), chunk_size):
    print(chunk)
